// Types
import { FETCH_PLACES_SUCCESS, GET_LOCATION, GET_LOCATION_SUCCESS } from "../actions/types";

// Initial State
const INITIAL_STATE = {
    isLocationLoading: true,
    isPlacesLoading: true,
    places: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_LOCATION:
            return INITIAL_STATE;
        case GET_LOCATION_SUCCESS:
            return {
                ...state,
                isLocationLoading: false,
                location: action.payload.location
            };
        case FETCH_PLACES_SUCCESS:
            return {
                ...state,
                isPlacesLoading: false,
                places: action.payload.places
            };
        default:
            return state;
    }
};