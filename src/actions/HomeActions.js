// Types
import { GET_LOCATION, GET_LOCATION_SUCCESS, FETCH_PLACES_SUCCESS } from "./types";
// Services
import { LocationService, GooglePlacesService } from "../services";

/*
Public
 */

export const getCurrentLocation = () => {
    return(dispatch) => {
        dispatch({type: GET_LOCATION});
        LocationService.getLocation()
            .then(location => {
                getNearbyPlaces(dispatch, location);
                dispatch({
                    type: GET_LOCATION_SUCCESS,
                    payload: { location }
                })
            })
            .catch(() => {});
    }
}

/*
Private
 */

const getNearbyPlaces = (dispatch, location) => {
    GooglePlacesService.fetchPlaces(location)
        .then(places => {
            dispatch({
                type: FETCH_PLACES_SUCCESS,
                payload: { places }
            })
        })
        .catch(() => {})
}
