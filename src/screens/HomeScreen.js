// React
import React from 'react';
import { StyleSheet, View, FlatList, Text, Button } from 'react-native';
import { connect } from 'react-redux';
import { StackNavigator } from 'react-navigation';
// Actions
import { getCurrentLocation } from '../actions';
// Components
import { Map, LoadingView, PlaceItem } from '../components';

// const { navigate } = this.props.navigation;

class HomeScreen extends React.Component {
  componentWillMount() {
    this.props.getCurrentLocation();
  }

  renderLoading() {
    return <LoadingView />;
  }

  renderMap() {
    const {
      containerStyle,
      mapStyle,
      placesListStyle,
      titleStyle,
      listStyle
    } = styles;

    const navigation = this.props.navigation;

    return (
      <>
        <View style={containerStyle}>
          <Map style={mapStyle} currentLocation={this.props.location} />
        </View>
        <View style={placesListStyle}>
          <FlatList
            style={listStyle}
            data={this.props.places}
            renderItem={({ item }) => (
              <PlaceItem
                {...item}
                onPress={() => navigation.navigate('PlaceDetails', { ...item })}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            ListHeaderComponent={
              <Text style={titleStyle}>Restaurants à proximité</Text>
            }
          />
        </View>
      </>
    );
  }

  render() {
    if (this.props.isLocationLoading && this.props.isPlacesLoading)
      return this.renderLoading();
    return this.renderMap();
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 2,
    backgroundColor: '#fff'
  },
  mapStyle: {
    flex: 1
  },
  placesListStyle: {
    flex: 1
  },
  titleStyle: {
    flex: 1,
    fontSize: 18,
    fontWeight: '700',
    padding: 10
  },
  listStyle: {
    flex: 1
  }
});

const mapStateToProps = state => {
  console.log(state.home);
  const { isLocationLoading, isPlacesLoading, location, places } = state.home;
  return { isLocationLoading, isPlacesLoading, location, places };
};

export default connect(
  mapStateToProps,
  {
    getCurrentLocation
  }
)(HomeScreen);
