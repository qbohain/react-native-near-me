import React from 'react';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';

class PlaceDetailsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('name')
    };
  };

  render() {
    const {
      containerStyle,
      imageStyle,
      infosStyle,
      placeNameStyle,
      textStyle,
      ratingStyle
    } = styles;

    const { navigation } = this.props;
    const name = navigation.getParam('name');
    const address = navigation.getParam('vicinity');
    const rating = navigation.getParam('rating');
    const photos = navigation.getParam('photos');

    let photoReference;

    if (photos) {
      photoReference = photos[0].photo_reference;
    } else {
      photoReference =
        'CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU';
    }

    const uri = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=375&photoreference=${photoReference}&key=AIzaSyCiaAFH6S_uhwWi0kw46ptsAWV6zFzxZvo`;

    return (
      <View style={containerStyle}>
        <Image style={imageStyle} source={{ uri: uri }} />
        <View style={infosStyle}>
          <Text style={placeNameStyle}>{name}</Text>
          <Text style={textStyle}>{address}</Text>
          <Text style={ratingStyle}>Note: {rating}/5</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start'
  },
  imageStyle: {
    height: 200,
    resizeMode: 'cover'
  },
  infosStyle: {
    padding: 10
  },
  placeNameStyle: {
    fontSize: 24,
    fontWeight: '700',
    marginBottom: 10
  },
  textStyle: {
    marginBottom: 10
  },
  ratingStyle: {
    marginBottom: 10
  }
});

export default PlaceDetailsScreen;
