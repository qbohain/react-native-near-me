import { createStackNavigator } from 'react-navigation';

// Screens
import HomeScreen from '../screens/HomeScreen';
import PlaceDetailsScreen from '../screens/PlaceDetailsScreen';

export const MainStackNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: 'Cheers Up',
        headerBackTitle: null
      }
    },
    PlaceDetails: {
      screen: PlaceDetailsScreen
    }
  },
  {
    initialRouteName: 'Home'
  }
);
