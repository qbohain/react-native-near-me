import { createAppContainer, createStackNavigator } from 'react-navigation';
import { MainStackNavigator } from './MainStackNavigator';

const AppContainer = createAppContainer(MainStackNavigator);

export default AppContainer;
