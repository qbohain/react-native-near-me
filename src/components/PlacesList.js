// React
import React from 'react';
import { StyleSheet, View, FlatList, Text } from 'react-native';
// Components
import { PlaceItem } from "./PlaceItem";

const PlacesList = ({ places })  => {

    const {
        containerStyle,
        titleStyle,
        listStyle,
    } = styles;

    return(
        <View style={containerStyle}>
            <FlatList
                style={listStyle}
                data={places}
                renderItem={ ({item}) => <PlaceItem {...item} /> }
                keyExtractor={(item, index) => index.toString()}
                ListHeaderComponent={<Text style={titleStyle}>Restaurants à proximité</Text>}
            />
        </View>
    );

};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1
    },
    titleStyle: {
        flex: 1,
        fontSize: 18,
        fontWeight: '700',
        padding: 10,
    },
    listStyle: {
        flex: 1,
    }
});

export { PlacesList };