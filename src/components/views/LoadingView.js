// React
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

const LoadingView  = () => {

    const { containerStyle } = styles;

    return(
        <View style={containerStyle}>
            <Text>Loading</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
});

export { LoadingView };