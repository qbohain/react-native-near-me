import React from 'react';
import { StyleSheet } from 'react-native';
import { MapView } from 'expo';

const Map = ({ currentLocation }) => {

    const {
        mapStyle
    } = styles;

    return <MapView
                style={mapStyle}
                provider="google"
                initialRegion={{
                    latitude: currentLocation.latitude,
                    longitude: currentLocation.longitude,
                    latitudeDelta: currentLocation.latitudeDelta,
                    longitudeDelta: currentLocation.longitudeDelta
                }}
            />

};

const styles = StyleSheet.create({
    mapStyle: {
        ...StyleSheet.absoluteFillObject,
    }
});

export { Map };