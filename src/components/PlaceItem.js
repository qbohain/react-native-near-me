import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

const PlaceItem = ({ name, photos, onPress }) => {
  const { containerStyle, imageStyle, placeNameStyle } = styles;

  let photoReference;

  if (photos) {
    photoReference = photos[0].photo_reference;
  } else {
    photoReference =
      'CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU';
  }

  const uri = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=150&photoreference=${photoReference}&key=AIzaSyCiaAFH6S_uhwWi0kw46ptsAWV6zFzxZvo`;

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={containerStyle}>
        <Image style={imageStyle} source={{ uri: uri }} />
        <Text style={placeNameStyle}>{name}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    marginHorizontal: 10,
    borderBottomWidth: 1,
    borderColor: '#e5e5e5'
  },
  imageStyle: {
    width: 80,
    height: 80,
    resizeMode: 'cover',
    borderRadius: 40
  },
  placeNameStyle: {
    flex: 1,
    marginLeft: 20
  }
});

export { PlaceItem };
