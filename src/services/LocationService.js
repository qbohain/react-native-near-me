import { Location, Permissions } from 'expo';

class LocationService {

    static getLocation() {
        return new Promise(async (resolve, reject) => {
            let {status} = await Permissions.askAsync(Permissions.LOCATION);
            if (status !== 'granted')
                reject('Persmission access to location denied');
            else {
                let location = await Location.getCurrentPositionAsync({});
                location = this.transformLocation(location);
                resolve(location);
            }
        });
    }

    static transformLocation(location) {
        // Todo : calculate latitudeDelta and longitudeDelta
        return {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            latitudeDelta: 0.0220,
            longitudeDelta: 0.0220
        }
    }

}

export { LocationService };