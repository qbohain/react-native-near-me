class GooglePlacesService {

    static fetchPlaces(location) {
        return new Promise((resolve, reject) => {
            fetch(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${location.latitude},${location.longitude}&radius=1000&type=restaurant&key=AIzaSyCiaAFH6S_uhwWi0kw46ptsAWV6zFzxZvo`)
                .then(res => res.json())
                .then(json => resolve(json.results))
                .catch(err => {
                    reject(err);
                })
        })
    }

}

export { GooglePlacesService };